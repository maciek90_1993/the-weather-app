package com.example.my_the_weather_app.helperClass.homeAdapter;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FeaturedHelperClass {

   private  int image;
   private String title, description;
}
