package com.example.my_the_weather_app;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LogIn extends AppCompatActivity {

    @BindView(R.id.loginEmail)
    TextInputLayout email;

    @BindView(R.id.loginPassword)
    TextInputLayout password;

    @BindView(R.id.loginLogoImage)
    ImageView loginLogoImage;

    @BindView(R.id.loginWelcome)
    TextView loginWelcome;

    @BindView(R.id.loginSlogan)
    TextView loginSlogan;

    @BindView(R.id.singUpButton)
    Button singUpButton;

    @BindView(R.id.loginButton)
    Button loginButton;

    @BindView(R.id.forgetPassowrd)
    Button forgetPassowrd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        singUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LogIn.this, SingUp.class);

                Pair[] pairs = new Pair[7];

                pairs[0] = new Pair<View, String> (loginLogoImage, "logo_image_dsc");
                pairs[1] = new Pair<View, String> (loginWelcome, "logo_text");
                pairs[2] = new Pair<View, String> (loginSlogan, "logo_desc");
                pairs[3] = new Pair<View, String> (email, "username_tran");
                pairs[4] = new Pair<View, String> (password, "password_tran");
                pairs[5] = new Pair<View, String> (loginButton, "singup_tran");
                pairs[6] = new Pair<View, String> (singUpButton, "login_singup_tran");

                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(LogIn.this, pairs);
                finish();
                startActivity(intent, options.toBundle());
            }
        });

        forgetPassowrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LogIn.this, SetNewPassword.class);
                startActivity(intent);
            }
        });
    }
}