package com.example.my_the_weather_app.user;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.*;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.my_the_weather_app.R;
import com.example.my_the_weather_app.helperClass.homeAdapter.FeaturedAdapter;
import com.example.my_the_weather_app.helperClass.homeAdapter.FeaturedHelperClass;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Dashboard extends AppCompatActivity {

    @BindView(R.id.feature_recycler)
    RecyclerView featureRecycler;

    RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                             WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        featureRecycler();
    }

    private void featureRecycler() {
        featureRecycler.setHasFixedSize(true);
        featureRecycler.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        ArrayList<FeaturedHelperClass> featuredLocations = new ArrayList<>();
        featuredLocations.add(new FeaturedHelperClass(
                R.drawable.cloudymaps,
                getResources().getString(R.string.theWeatherMap),
                getResources().getString(R.string.mapTheWeatherDescription)));
        featuredLocations.add(new FeaturedHelperClass(
                R.drawable.alert,
                getResources().getString(R.string.alertsInfo),
                getResources().getString(R.string.alertsInfoDesc)));
        featuredLocations.add(new FeaturedHelperClass(
                R.drawable.high_temperature,
                getResources().getString(R.string.lastTheWeather),
                getResources().getString(R.string.lastTheWeatherInfo)));

        adapter = new FeaturedAdapter(featuredLocations, -1, getApplicationContext());
        featureRecycler.setAdapter(adapter);
        final int speedScroll = 2200;
        Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            int count = 0;
            boolean flag = true;

            @Override
            public int hashCode() {
                return super.hashCode();
            }

            @Override
            public void run() {
                if(count < adapter.getItemCount()){
                    if(count==adapter.getItemCount()-1){
                        flag = false;
                    }else if(count == 0){
                        flag = true;
                    }
                    if(flag) count++;
                    else count--;

                    featureRecycler.smoothScrollToPosition(count);
                    handler.postDelayed(this,speedScroll);
                }
            }
        };

        handler.postDelayed(runnable,speedScroll);


    }
}