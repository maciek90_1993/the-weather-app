package com.example.my_the_weather_app;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SingUp extends AppCompatActivity {

    @BindView(R.id.singupImageView)
    ImageView singupImageView;

    @BindView(R.id.singupWelcome)
    TextView singupWelcome;

    @BindView(R.id.registartionEmail)
    TextInputLayout registartionEmail;

    @BindView(R.id.singupButtonLogin)
    Button singupButtonLogin;

    Animation slideLeftToRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);
        ButterKnife.bind(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        slideLeftToRight = AnimationUtils.loadAnimation(this, R.anim.slide_left_to_right);
        singupImageView.setAnimation(slideLeftToRight);

       singupButtonLogin.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(SingUp.this, LogIn.class);
               Pair[] pairs = new Pair[2];
               pairs[0] = new Pair<View, String>(singupImageView, "logo_image");
               pairs[1] = new Pair<View, String>(singupWelcome, "logo_text");

               ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                       SingUp.this, pairs);
               finish();
               startActivity(intent, options.toBundle());
           }
       });
    }
}